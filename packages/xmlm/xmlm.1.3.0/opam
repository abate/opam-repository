opam-version: "2.0"
maintainer: "Daniel Bünzli <daniel.buenzl i@erratique.ch>"
authors: ["Daniel Bünzli <daniel.buenzl i@erratique.ch>"]
homepage: "http://erratique.ch/software/xmlm"
dev-repo: "git+http://erratique.ch/repos/xmlm.git"
bug-reports: "https://github.com/dbuenzli/xmlm/issues"
doc: "http://erratique.ch/software/xmlm/doc/Xmlm"
tags: [ "xml" "codec" "org:erratique" ]
license: "ISC"
depends: [
  "ocaml" {>= "4.02.0"}
  "ocamlfind" {build}
  "ocamlbuild" {build}
  "topkg" {build & >= "0.9.0"}
]
build:
[[
   "ocaml" "pkg/pkg.ml" "build"
           "--dev-pkg" "%{pinned}%"
]]
synopsis: "Streaming XML codec for OCaml"
description: """
Xmlm is a streaming codec to decode and encode the XML data format. It
can process XML documents without a complete in-memory representation of the
data.

Xmlm is made of a single independent module and distributed
under the ISC license."""
url {
  src: "http://erratique.ch/software/xmlm/releases/xmlm-1.3.0.tbz"
  checksum: [
    "md5=d63ce15d913975211196b5079e86a797"
    "sha256=1675f0c39d39130e7778aeadb162ef7badee71832bf9ab5ec5ea2738cbeb2de7"
    "sha512=fd3b9ba8cd12321fd8d56e9ce829a7c8b121bff11cbf7ede58fa0280302fe0ba64f510360ff5b5182591faef660792db66db36e45f6355e3d22bb521f8e308fe"
  ]
}
